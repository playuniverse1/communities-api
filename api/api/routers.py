from rest_framework import routers

from accounts.views import UserViewSet, ConfigViewSet, CommunityViewSet, RequestViewSet, UserModelViewSet


router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'profiles', UserModelViewSet)
router.register(r'config', ConfigViewSet)
router.register(r'requests', RequestViewSet)
router.register(r'communities', CommunityViewSet)