from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView

from django.conf import settings

from accounts.views import get_fennek_panel, create_request

from .routers import router
from .docs import schema_view
from django.conf.urls.static import static

# Prefix to be used on all v1 API urls
v1_prefix = 'v1/'
def v1_url(url):
    # Prepend a url string with the v1 prefix.
    return v1_prefix + url

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='admin/')),

    # API urls
    path(v1_url('docs/'), schema_view.with_ui('redoc', cache_timeout=0),
         name='api-docs'),
    path(v1_prefix, include(router.urls)),
    path(f'{v1_prefix}auth/', include('djoser.urls')),
    path(f'{v1_prefix}auth/', include('djoser.urls.jwt')),
    path(f'{v1_prefix}fennek', get_fennek_panel, name="get_fennek_panel"),
    path(f'{v1_prefix}create-request', create_request, name="create_request") 

]

if settings.STAGE != "development":
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.MEDIA_ROOT)