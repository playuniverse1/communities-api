from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Community, Config, Request, User, ServerVersion 


admin.site.register(User, UserAdmin)
admin.site.register(Config)
admin.site.register(Request)
admin.site.register(Community)
admin.site.register(ServerVersion)