from rest_framework.serializers import ModelSerializer
from rest_framework import fields, serializers
from .models import Community, Config, Request, User 
from datetime import date, datetime


class CustomUserSerializer(ModelSerializer):
    class Meta(object):
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'id', 'is_staff')


class ConfigSerializer(ModelSerializer):
    class Meta(object):
        model = Config
        fields = ('__all__')


class RequestSerializer(ModelSerializer):
    class Meta(object):
        model = Request
        fields = ('id', 'created_at', 'updated_at', 'status', 'runtime', 'memory', 'slots', 'server_ip', 'server_host', 'server_port', 'server_id', 'server_friendly_id', 'user', 'community', 'queue')


class CommunitzSerializer(ModelSerializer):
    class Meta(object):
        model = Community
        fields = ('id', 'user', 'homepage', 'discord', 'name', 'description', 'email')