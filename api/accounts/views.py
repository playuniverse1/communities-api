from os import name
from django.contrib.auth.models import Permission
from django.http import request
from django_q.tasks import async_task
from rest_framework import permissions, serializers
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet, ReadOnlyModelViewSet, ViewSet

from django.conf import settings
from .models import Community, Config, Request, ServerVersion, User
from .serializers import CommunitzSerializer, ConfigSerializer, CustomUserSerializer, RequestSerializer
from rest_framework.generics import ListAPIView
from rest_framework.decorators import api_view, permission_classes

from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.template.loader import render_to_string

import json
import requests
import ast
import uuid

class IsOwnerOrStaffOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.user.is_staff: 
            return True
        # try:
        #     if request.user.is_staff: 
        #         return True
        # except AttributeError:
        #     return False
	
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        try:
            if request.method in ["PATCH", "POST", "DELETE"]:
                if request.user.is_staff: 
                    return True
                else:
                    False
            return obj.user == request.user
        except TypeError:
            return False
       


class UserViewSet(GenericViewSet):

    queryset = User.objects.all()
    serializer_class = CustomUserSerializer

    @action(detail=False, permission_classes=[IsAuthenticated])
    def me(self, request):
        serializer = self.serializer_class(request.user)
        return Response(serializer.data)


class UserModelViewSet(ModelViewSet):
	
    queryset = User.objects.all()
    serializer_class = CustomUserSerializer

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)

    def get_object(self):
        return User.objects.get(id=self.request.user.id)



class ConfigViewSet(ModelViewSet):

    queryset = Config.objects.all()
    serializer_class = ConfigSerializer


class RequestViewSet(ModelViewSet):
    permission_classes = (IsOwnerOrStaffOrReadOnly,)
    queryset = Request.objects.all()
    serializer_class = RequestSerializer

    def get_queryset(self):
        q_set = Request.objects.all()
        try:
            if self.request.user.is_staff:
                return q_set.order_by("status")
            else: return q_set.filter(user=self.request.user)
        except:
            return q_set(user=self.request.user)

    def perform_destroy(self, instance):
        instance = self.get_object()
        headers = {'Content-Type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer '+settings.FENNEK_API_TOKEN, 'Accept': 'application/json'}
        try:
            requests.delete(
                url=settings.FENNEK_API+"/application/servers/"+str(instance.server_id)+"/true",
                headers=headers,
            )
            user = requests.get(
                    url=settings.FENNEK_API+"/application/users/external/"+str(instance.id),
                    headers=headers, 
                ).json()["attributes"]["id"]
            requests.delete(
                url=settings.FENNEK_API+"/application/users/"+str(user),
                headers=headers,
            )
        except KeyError as e:
            print(e)
        return super().perform_destroy(instance)
        

    def perform_update(self, serializer):
        status = self.request.data.get("status", None)
        installation = self.request.data.get("installation")
        instance = self.get_object()
        if status == 1:
            # send notification to user
            pass
        if status == 2:
            # waiting for answer
            pass
        if status == 3 and not installation:
            # request accepted & create user & send instructions
            user = instance.user
            headers = {'Content-Type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer '+settings.FENNEK_API_TOKEN, 'Accept': 'application/json'}
            response = requests.post(
                url=settings.FENNEK_API+"/application/users",
                headers=headers,
                data=json.dumps({
                    "email": user.email,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "username": str(uuid.uuid1()),
                    "external_id": str(instance.id)
                })
            )
            params = {
                'name': user.first_name,
                'link': f'{settings.SITE_URL}',
                'text': "Deine Anfrage wurde bestätigt."
            }

            msg_plain = render_to_string(f'mail.txt', params)
            msg_html = render_to_string(f'mail.html', params)

            send_mail(
                "🔔 Deine Anfrage wurde bestätigt | PlayUniverse communities",
                msg_plain,
                'PlayUniverse communties <no-reply@playuniverse.eu>',
                [user.email],
                html_message=msg_html,
            )
        if status == 4:
            # request declined
            pass
        if status == 5:
            # finished
            pass
        if status == 6:
            # caceled
            pass

        if status == 3 and installation:
            choosen_egg = False
            for egg in get_minecraft_eggs():
                if egg["attributes"]["uuid"] == installation:
                    choosen_egg = egg["attributes"]
                    break

            allocation = get_allocation()
            if not allocation or not choosen_egg:
                raise NotFound
            

            # create server instance
            headers = {'Content-Type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer '+settings.FENNEK_API_TOKEN, 'Accept': 'application/json'}
            user = requests.get(
                url=settings.FENNEK_API+"/application/users/external/"+str(instance.id),
                headers=headers, 
            ).json()["attributes"]
            data = {
                    "name": str(instance.id),
                    "user": user["id"],
                    "egg": choosen_egg["id"],
                    "docker_image": ServerVersion.objects.get(egg=choosen_egg["id"]).docker_image,
                    "startup": choosen_egg["startup"],
                    "environment": ast.literal_eval(ServerVersion.objects.get(egg=choosen_egg["id"]).variables),
                    "feature_limits": {
                        "databases": 0
                    },
                    "limits":  {
                        "memory": instance.memory*1024,
                        "swap": 0,
                        "disk": 2048,
                        "io": 500,
                        "cpu": 0 
                    },
                    "allocation": {
                        "default": allocation["id"]
                    }
                }
            response = requests.post(
                url=settings.FENNEK_API+"/application/servers",
                headers=headers,
                data=json.dumps(data)
            )
            try:
                serializer.save(server_ip=allocation["ip"], server_port=allocation["port"], server_host=allocation["alias"], server_id=response.json()["attributes"]["id"], server_friendly_id=response.json()["attributes"]["identifier"])
                params = {
                    'name': user["first_name"],
                    'link': f'{settings.SITE_URL}',
                    'text': "Dein Server wurde erstellt."
                }

                msg_plain = render_to_string(f'mail.txt', params)
                msg_html = render_to_string(f'mail.html', params)

                send_mail(
                    "🔔 Dein Server wurde erstellt| PlayUniverse communities",
                    msg_plain,
                    'PlayUniverse communties <no-reply@playuniverse.eu>',
                    [user["email"]],
                    html_message=msg_html,
                )
            except KeyError as e:
                raise NotFound
        
        return super().perform_update(serializer)


class CommunityViewSet(ModelViewSet):

    queryset = Community.objects.all()
    serializer_class = CommunitzSerializer
    permission_classes = (IsOwnerOrStaffOrReadOnly,)


def get_minecraft_eggs():
    headers = {'Content-Type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer '+settings.FENNEK_API_TOKEN, 'Accept': 'application/json'}

    eggs = requests.get(
        url=settings.FENNEK_API+"/application/categories/3/eggs",
        headers=headers,
    ).json()["data"]
    final = []
    for egg in eggs:
        versions = ServerVersion.objects.all()
        for ver in versions:
            if egg["attributes"]["id"] == ver.egg:
                egg["attributes"]["name"] = egg["attributes"]["name"]+" - "+ver.name
                final.append(egg)
    return final



def get_allocation():
    headers = {'Content-Type': 'application/json; charset=UTF-8', 'Authorization': 'Bearer '+settings.FENNEK_API_TOKEN, 'Accept': 'application/json'}

    for node in requests.get(
        url=settings.FENNEK_API+"/application/nodes/",
        headers=headers,
    ).json()["data"]:
        allocations = requests.get(
            url=settings.FENNEK_API+"/application/nodes/"+str(node["attributes"]["id"])+"/allocations",
            headers=headers,
        ).json()["data"]
        for allocation in allocations:
            if allocation["attributes"]["assigned"] == False:
                return allocation["attributes"]
        return None


@api_view(["GET"])
def get_fennek_panel(request):
    action = request.query_params.get("action")
    if "get_eggs" in action:
        eggs = get_minecraft_eggs()
        if action == "get_eggs_min":
            r = []
            for egg in eggs:
                r.append({
                    "name": egg["attributes"]["name"],
                    "id": egg["attributes"]["id"],
                    "uuid": egg["attributes"]["uuid"],
                    "category": egg["attributes"]["category"],
                })
            return Response(data=r, status=200)
        return Response(data=eggs, status=200)
                

@api_view(["POST"])
def create_request(request):
    data = request.data
    try:
        user = User.objects.get(email=data["email"])
    except User.DoesNotExist:
        u_res = requests.post(
            f'{settings.SITE_URL}v1/auth/users/',
            headers={ 
             'Content-Type': 'application/json'
            },
            data=json.dumps({
                "email": data["email"],
                "username": data["email"],
                "password": data["password"]
            })
        )
        user = User.objects.get(email=data["email"])
        user.first_name = data["first_name"]
        user.last_name = data["last_name"]
        user.save()
        # user = User.objects.create(
        #     username=data["email"],
        #     email=data["email"],
        #     first_name=data["first_name"],
        #     last_name=data["last_name"]
        # )
    community = Community.objects.create(
        name=data["community_name"],
        homepage=data["homepage"],
        description=data["description"],
        user=user
    )
    request = Request.objects.create(
        user=user,
        community=community,
        memory=data["mem"],
        slots=data["slots"]
    )
    return Response(status=201)
