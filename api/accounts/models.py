from django.db import models

from django.contrib.auth.models import AbstractUser
import uuid

from django.db.models.fields import CharField, TextField
from django.db.models.fields.related import ForeignKey
from django.db.models.signals import post_save, post_delete

from django_q.tasks import async_task

class User(AbstractUser):
    discord = models.CharField(default="", blank=True, max_length=250)


class Community(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    id = models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, unique=True, primary_key=True)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, blank=True)
    homepage = models.CharField(default="", max_length=500)
    discord = models.CharField(default="", blank=True, max_length=500)
    name = models.CharField(default="", blank=False, max_length=150)
    description = models.TextField(default="", blank=False, max_length=2500)

    @property
    def email(self):
        return self.user.email


class Request(models.Model):
    REQUEST_STATUS = ((0, 'OPEN'), (1, 'IN_REVIEW'), (2, 'WAITING_ANSWER'), (3, 'APPROVED'), (4, 'DECLINED'), (5, 'FINISHED'), (6, 'CANCEL'), )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    id = models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, unique=True, primary_key=True)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, blank=True)
    status = models.IntegerField(default=0, choices=REQUEST_STATUS)
    runtime = models.IntegerField(default=1, blank=False)
    community = models.ForeignKey(Community, on_delete=models.CASCADE)
    memory = models.IntegerField(default=2048, blank=False)
    slots = models.IntegerField(default=100, blank=False)
    server_ip = models.CharField(default="", blank=True, max_length=50)
    server_host = models.CharField(default="", blank=True, max_length=250)
    server_port = models.IntegerField(default=0, blank=True)
    server_id = models.IntegerField(default=0, blank=True)
    server_friendly_id = models.CharField(default="", blank=True, max_length=50)

    @property
    def queue(self):
        open_requests = Request.objects.filter(status__in=[0,1,2]).order_by('created_at')
        counter = 1
        for r in open_requests:
            if r.id == self.id:
                return counter
            counter = counter + 1

        return -1




class Config(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    min_mem = models.IntegerField(default=0, blank=False)
    max_mem = models.IntegerField(default=16384, blank=False)
    min_slots = models.IntegerField(default=25, blank=False)
    max_slots = models.IntegerField(default=5000, blank=False)
    form_enabled = models.BooleanField(default=False)


class ServerVersion(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    id = models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, unique=True, primary_key=True)
    name = models.CharField(max_length=50)
    egg = models.IntegerField(blank=False)
    variables = models.TextField(blank=True, max_length=1500)
    docker_image = models.CharField(max_length=250, blank=True, default="")
